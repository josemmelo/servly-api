<?php

use Illuminate\Http\Request;

Route::post('/users/authorize', ['uses' => 'UsersController@authorizeClient'])->middleware('log');
Route::get('applications/{appId}/information', ['uses' => 'ApplicationsController@information'])->middleware('log');
Route::post('users', 'UsersController@store');

Route::get('software_client/download', 'SoftwareClientController@download');

Route::middleware('oauth')->group(function () {
    Route::get('users/me', function (Request $request) {
        return \Servly\Models\User::where('id', $request->user()->id)
            ->with('subscriptions')
            ->first();
    });

    Route::put('users', 'UsersController@update');

    include 'user/logs.php';

    include 'user/nodes.php';

    include 'user/applications.php';

    include 'user/informations.php';

    include 'user/subscriptions.php';

    Route::get('software_client', 'SoftwareClientController@list');
});


Route::prefix('admin')->middleware('admin')->group(function () {
    include 'admin/available_services.php';

    include 'admin/application_types.php';

    include 'admin/users.php';

    include 'admin/logs.php';

    include 'admin/software_clients.php';
});
