<?php


Route::resource('available_services', 'AvailableServicesController', [
    'only' => [
        'store', 'update', 'index', 'destroy'
    ]
]);