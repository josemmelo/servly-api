<?php


Route::resource('application_types', 'ApplicationsTypesController', [
    'only' => [
        'index', 'update', 'store', 'destroy'
    ]
]);