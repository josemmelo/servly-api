<?php

Route::get('users', 'UsersController@index');

Route::get('users/active', 'UsersController@active');