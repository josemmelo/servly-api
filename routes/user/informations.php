<?php


Route::resource('application_types', 'ApplicationsTypesController', [
    'only' => [
        'index'
    ]
]);

Route::resource('available_services', 'AvailableServicesController', [
    'only' => [
        'index'
    ]
]);