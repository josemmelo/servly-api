<?php


Route::resource('applications', 'ApplicationsController', [
    'only' => [
        'index', 'show', 'destroy', 'update',
    ]]);

Route::get('applications/{appId}/logs/stats', 'ApplicationLogController@stats');

Route::resource('applications/{appId}/logs', 'ApplicationLogController', [
    'only' => [
        'store', 'index'
    ]]);