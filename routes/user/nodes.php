<?php

Route::resource('nodes', 'NodesController', [
    'only' => [
        'update', 'show', 'index', 'store', 'destroy',
    ]]);

Route::resource('nodes.applications', 'ApplicationsController', [
    'only' => [
        'index', 'store',
    ]]);
