<?php



Route::get('logs/stats', 'ApplicationLogController@stats');

Route::resource('logs', 'ApplicationLogController', [
    'only' => [
        'index'
    ]]);