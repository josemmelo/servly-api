FROM shakyshane/laravel-php:latest

COPY ./composer.lock ./composer.json /var/www/

WORKDIR /var/www

RUN docker-php-ext-install pdo_mysql gd bcmath

COPY ./ /var/www

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && php composer.phar install --no-dev --no-scripts \
    && rm composer.phar


RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN chmod -R 777 /var/www/public

RUN php artisan optimize
