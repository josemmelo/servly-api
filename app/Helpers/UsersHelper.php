<?php

namespace Servly\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;
use Servly\Models\User;

class UsersHelper
{
    public static function authorize($clientId, $clientSecret)
    {
        return User::where('client_id', $clientId)
            ->where('client_secret', $clientSecret)
            ->first();

    }

    public static function create($userInfo)
    {
        if(!RequestValidatorHelper::validateParameters($userInfo, User::$requiredParamsToCreate))
            return response()->json(['error_msg' => 'Missing parameters.', 'error_code' => '400']);

        $userInfo['password'] = Hash::make($userInfo['password']);
        $userInfo['client_id'] = Uuid::uuid4();
        $userInfo['client_secret'] = Uuid::uuid4();

        return User::create($userInfo);
    }

    public static function update($user, $userInfo)
    {
        if (array_has($userInfo, 'password'))
            $userInfo['password'] = Hash::make($userInfo['password']);

        return $user->update($userInfo);
    }

    public static function index(Request $request, $page = null)
    {

        $users = Users::paginate(15);

    }
}