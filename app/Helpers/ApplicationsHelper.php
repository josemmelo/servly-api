<?php

namespace Servly\Helpers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Servly\Models\Application;
use Servly\Models\ApplicationLog;
use Servly\Models\ApplicationLogFilters;
use Servly\Models\ApplicationType;
use Servly\Models\AvailableServices;
use Servly\Models\Log;
use Servly\Models\Node;

class ApplicationsHelper
{
    public static function index($userId, $nodeId = null)
    {
        $query = self::applicationJoinsQuery($userId);

        if ($nodeId != null)
            $query = $query->where('nodes.id', $nodeId);

        $applications = $query->select('applications.*', 'application_info.*', 'application_log_filters.filters')
            ->paginate(10);

        foreach ($applications as $application)
            self::decodeApplicationJsonElements($application);

        return $applications;
    }

    public static function applicationJoinsQuery($userId)
    {
        return Application::join('nodes', 'nodes.id', '=', 'applications.node_id')
            ->join('users', 'nodes.user_id', '=', 'users.id')
            ->join('application_info', 'application_info.application_id', '=', 'applications.id')
            ->join('application_log_filters', 'application_log_filters.application_id', '=', 'applications.id')
            ->join('application_types', 'application_types.id', '=', 'applications.type_id')
            ->where('users.id', $userId);
    }

    public static function decodeApplicationJsonElements($application)
    {
        $application->information = json_decode($application->information);
        $application->filters = json_decode($application->filters);
        $application->active_services = json_decode($application->active_services);
        return $application;
    }

    public static function show($userId, $applicationId)
    {
        $application = self::applicationJoinsQuery($userId)
            ->where('applications.id', $applicationId)
            ->select('applications.*', 'application_info.*', 'application_log_filters.filters',
                     'application_types.type', 'application_types.description as type_description')
            ->first();

        if ( ! $application)
            return response('application_not_found', 401);

        return self::decodeApplicationJsonElements($application);
    }

    public static function store($applicationInformation, $nodeId)
    {
        if ( ! RequestValidatorHelper::validateParameters($applicationInformation, Application::$requiredParamsToCreate))
            return response()->json(['error_msg' => 'Missing parameters.', 'error_code' => '400'], 400);

        try {
            DB::beginTransaction();

            if (count(AvailableServices::whereIn('identifier', $applicationInformation['active_services'])->get()) == 0)
                return response()->json(['error_msg' => 'Invalid service.', 'error_code' => '400'], 400);

            $type = ApplicationType::where('type', $applicationInformation['type'])
                ->where('description', $applicationInformation['type_description'])
                ->first();

            $appInformation = [
                'node_id'         => $nodeId,
                'name'            => $applicationInformation['name'],
                'type_id'         => $type->id,
                'technology'      => $applicationInformation['technology'],
                'regex'           => $applicationInformation['regex'],
                'active_services' => json_encode($applicationInformation['active_services']),
            ];

            $application = Application::create($appInformation);

            $application->application_info()
                ->create();

            $filters = ApplicationLogFilters::$defaultFilters;
            if (array_key_exists('filters', $applicationInformation)) {
                $filters = $applicationInformation['filters'];
            }

            $application->application_filters()
                ->create([
                             'filters' => json_encode($filters),
                         ]);

            $application->active_services = json_decode($application->active_services);

            DB::commit();

            return $application;
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json([
                                        'error_msg'  => $exception->getMessage(),
                                        'error_code' => $exception->getCode(),
                                    ], 400);
        }
    }

    public static function update(Request $request, $id)
    {
        $application = Application::find($id);

        if ( ! $application)
            return response()->json(['error_msg' => 'Application not found.'], 400);

        DB::transaction(function () use ($request, $application) {
            $updateArray = [];

            $updateArray['name'] = $request->has('name') ? $request->get('name') : $application->name;
            $updateArray['up_time'] = $request->has('up_time') ? $request->get('up_time') : $application->up_time;
            $updateArray['technology'] = $request->has('technology') ? $request->get('technology') : $application->technology;
            $updateArray['active_services'] = $request->has('active_services') ? json_encode($request->get('active_services')) : $application->active_services;

            if ($request->has('type') && $request->has('type_description')) {
                $type = ApplicationType::where('type', $request->get('type'))
                    ->where('description', $request->get('type_description'))
                    ->first();

                if ($type != null)
                    $updateArray['type_id'] = $type->id;
            }

            $application->update($updateArray);

            $updateArray = [];
            if ($request->has('information')) {
                $information = $request->get('information');
                if ($information != null) {
                    foreach ($information as $key => $value) {
                        $updateArray['information->' . $key] = $value;
                    }
                }
            }

            $updateArray['total_requests'] = $request->has('total_requests') ? $request->get('total_requests') : $application->application_info->total_requests;
            $updateArray['total_errors'] = $request->has('total_errors') ? $request->get('total_errors') : $application->application_info->total_errors;

            $application->application_info()->update($updateArray);

            $filtersArray = [];
            if ($request->has('filters')) {
                $filtersArray['filters'] = json_encode($request->get('filters'));
            }

            $application->application_filters()->update($filtersArray);
        });


        return $application;
    }


    public static function information($appId)
    {
        Application::where('id', $appId)->update(['up_time' => round(microtime(true) * 1000)]);

        $application = Application::join('application_log_filters', 'application_id', '=', 'applications.id')
            ->select('applications.regex as regex',
                     'applications.active_services  as active_services',
                     'application_log_filters.filters as filters')
            ->where('applications.id', $appId)
            ->first();

        $application->active_services = json_decode($application->active_services);
        $application->filters = json_decode($application->filters);
        try{
            $application->log_level = $application->filters->log_level;
        }catch(\Exception $e){
            $application->log_level = [];
        }

        return $application;
    }

    public static function getLogs($userId, $appId, $query)
    {
        $dbQuery = ApplicationLog::join('applications', 'application_id', '=', 'applications.id')
            ->join('nodes', 'applications.node_id', '=', 'nodes.id')
            ->join('users', 'nodes.user_id', '=', 'users.id')
            ->select('application_logs.*')
            ->where('users.id', $userId);

        if ($appId != null)
            $dbQuery = $dbQuery->where('application_id', $appId);

        if ($query != null)
            $dbQuery = $dbQuery->where(function ($q) use ($query) {
                $q->where('message', 'like', '%' . $query . '%')
                    ->orWhere('log_type', '=', $query);
            });

        return $dbQuery->orderBy('date', 'DESC')->orderBy('time', 'DESC')->paginate(10);
    }

    public static function getLogsStats($user, $appId)
    {
        $dbQuery = ApplicationLog::join('applications', 'application_id', '=', 'applications.id')
            ->join('nodes', 'applications.node_id', '=', 'nodes.id')
            ->join('users', 'nodes.user_id', '=', 'users.id')
            ->where('users.id', $user);

        if ($appId != null)
            $dbQuery = $dbQuery->where('application_id', $appId);

        $stats = $dbQuery->select('log_type',
                                  DB::raw('applications.name as app_name'),
                                  DB::raw('count(app) as counter'),
                                  DB::raw('month(date) as month'),
                                  DB::raw('year(date) as year'))
            ->groupBy(['log_type', 'app_name', 'month', 'year'])
            ->orderBy('year', 'DESC')->orderBy('month', 'DESC')
            ->get();

        $statsToReturn = [];

        foreach ($stats as $stat) {
            $statsToReturn[$stat->month . '-' . $stat->year][$stat->app_name][$stat->log_type] = [
                'total' => $stat->counter,
            ];
        }


        return $statsToReturn;
    }
}