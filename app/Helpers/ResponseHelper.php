<?php

function error_response($error_msg, $error_code) {
    return response()->json(['error_msg' => $error_msg, 'error_code' => $error_code], $error_code);
}

function success_response($data, $code) {
    return response()->json($data, $code);
}