<?php

namespace Servly\Helpers;

use Illuminate\Http\Request;
use Servly\Models\Node;
use Servly\Models\User;

class NodesHelper
{
    public static function store($user, $node)
    {
        $node['os'] = "";
        return $user->nodes()->create($node);
    }

    public static function update(Request $request, $id)
    {
        $updateArray = [];
        $insertInfo = false;

        if ($request->has('Host')) {
            $host = json_decode($request->get('Host'));

            $updateArray['up_time'] = $host->uptime;
            $updateArray['os'] = $host->os;
            $insertInfo = true;
        }
        if ($request->has('VirtualMemory')) {
            $memory = json_decode($request->get('VirtualMemory'));

            $updateArray['memory_max_usage'] = $memory->used;
            $insertInfo = true;
        }

        if ($request->has('CpuUsage')) {
            $updateArray['cpu_max_usage'] = $request->get('CpuUsage')[0];
            $insertInfo = true;
        }

        if ($request->has('info')) {
            $updateArray['info'] = json_encode($request->get('info'));
        }
        if ($request->has('name')) {
            $updateArray['name'] = $request->get('name');
        }
        if ($request->has('ip')) {
            $updateArray['ip'] = $request->get('ip');
        }
        if ($request->has('machine_name')) {
            $updateArray['machine_name'] = $request->get('machine_name');
        }

        return Node::where('id', $id)
            ->update($updateArray);
    }

    public static function show($user, $id)
    {
        $node = Node::where('id', $id)
            ->where('user_id', $user->id)
            ->with('applications')
            ->with('monitored_nodes')
            ->first();

        $node->info = json_decode($node->info);
        return $node;
    }

    public static function delete($user, $id)
    {
        return Node::where('id', $id)->where('user_id', $user->id)->delete();
    }
}