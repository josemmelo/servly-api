<?php

function saveToLog(\Illuminate\Http\Request $request)
{
    $log = [
        'method'          => $request->method(),
        'headers'         => $request->headers,
        'parameters'      => $request->all(),
        'url'             => $request->url(),
        'user_agent'      => $request->userAgent(),
        'user_ip_address' => $request->ip(),
        'user'            => $request->user(),
    ];

    \Servly\Models\Log::firstOrCreate([
                                          'content' => json_encode($log),
                                      ]);
}


function saveLog(\Servly\Models\Log $log)
{
    $log->save();
}