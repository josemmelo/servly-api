<?php

namespace Servly\Http\Middleware;


use Servly\Jobs\LogHttpRequests;

class RequestLogMiddleware
{
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        $elapsedTime = round(microtime(true) * 1000);

        $response = $next($request);

        $elapsedTime = round(microtime(true) * 1000) - $elapsedTime;

        $requestInfo = [
            'method'          => $request->method(),
            'headers'         => $request->headers,
            'parameters'      => $request->all(),
            'url'             => $request->url(),
            'user_agent'      => $request->userAgent(),
            'user_ip_address' => $request->ip(),
            'user'            => null,
        ];

        LogHttpRequests::dispatch($requestInfo, $response, $elapsedTime);

        return $response;
    }
}