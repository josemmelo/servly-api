<?php

namespace Servly\Http\Middleware;


use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Servly\Jobs\LogHttpRequests;
use Servly\Models\DataProcessor;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $elapsedTime = round(microtime(true) * 1000);

        if($this->isDataProcessor($request)) {
            $response = $next($request);
        }
        else if ( $request->user() ) {
            $response = $next($request);
        } else {
            $response = response('Unauthorized.', 401);
        }

        $elapsedTime = round(microtime(true) * 1000) - $elapsedTime;

        $requestInfo = [
            'method'          => $request->method(),
            'headers'         => $request->headers,
            'parameters'      => $request->all(),
            'url'             => $request->url(),
            'user_agent'      => $request->userAgent(),
            'user_ip_address' => $request->ip(),
            'user'            => $request->user(),
        ];

        try{
            LogHttpRequests::dispatch(json_encode($requestInfo), $response, $elapsedTime);
        }catch (\Exception $e){
            LogHttpRequests::dispatch($requestInfo, $e->getMessage(), $elapsedTime);
        }

        return $response;
    }

    private function isDataProcessor(Request $request)
    {
        $authorizationHeader = $request->header('Authorization');
        $authorizationToken = str_replace("processor ", "", $authorizationHeader);

        if($authorizationToken == $authorizationHeader)
            return false;

        return DataProcessor::where('token', $authorizationToken)
            ->where('ip', $request->ip())
            ->first();
    }
}