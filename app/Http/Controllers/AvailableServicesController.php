<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Servly\Helpers\UsersHelper;
use Servly\Models\AvailableServices;

class AvailableServicesController extends Controller
{
    public function index(Request $request)
    {
        return AvailableServices::all();
    }

    public function update(Request $request, $id)
    {
        try {
            $availableService = AvailableServices::where('id', $id)->update($request->all());
            return response()->json($availableService, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function destroy(Request $request, $typeId)
    {
        return AvailableServices::where('id', $typeId)->delete();
    }

    public function store(Request $request)
    {
        $availableServices = [
            'name'        => $request->get('name'),
            'identifier'  => $request->get('identifier'),
            'description' => $request->get('description'),
        ];
        try {
            $available = AvailableServices::create($availableServices);

            return response()->json($available, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }
}
