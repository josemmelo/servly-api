<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Servly\Helpers\UsersHelper;
use Servly\Models\User;

class UsersController extends Controller
{
    public function authorizeClient(Request $request)
    {
        $user = UsersHelper::authorize($request->get('client_id'), $request->get('client_secret'));

        if ( ! $user) {
            return error_response('Client authorization failed.', 401);
        }

        return success_response(['error' => false], 200);
    }

    public function store(Request $request)
    {
        return UsersHelper::create($request->all());
    }

    public function update(Request $request)
    {
        return UsersHelper::update($request->user(), $request->all()) ? response()->json(['updated' => true]) : response(['updated' => false], 404);
    }

    public function index(Request $request)
    {
        $q = $request->has('q') ? $request->get('q') : null;

        if(! $q)
            return User::with('nodes')->with('subscriptions')->orderBy('id')->paginate(10);
    }

    public function active() {
        return DB::table('active_users')->orderBy('user_id')->paginate(20);
    }
}
