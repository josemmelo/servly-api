<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Servly\Models\SoftwareClient;

class SoftwareClientController extends Controller
{
    public function list()
    {
        return SoftwareClient::all();
    }

    public function download(Request $request)
    {
        $softwareClient = null;

        if ($request->has('version')) {
            $softwareClient = SoftwareClient::where('version', $request->get('version'))->first();
        }

        if ( ! $softwareClient) {
            $softwareClient = SoftwareClient::orderBy('created_at', 'DESC')->first();
        }


        $url = storage_path('app') . '/' . SoftwareClient::$storageLocation . '/client-' . $softwareClient->version . '.zip';

        return response()->download($url, 'client-'. $softwareClient->version . '.zip');
    }

    public function store(Request $request)
    {
        $requestFileName = 'software_client_file';

        if ( ! $request->has('description') || ! $request->has('version'))
            return response()
                ->json(['error_msg' => 'Missing description/version.', 'error_code' => '401'])
                ->status(400);

        if ( ! $request->hasFile($requestFileName) || ! $request->file($requestFileName)->isValid()) {
            return response()
                ->json(['error_msg' => 'Software client file missing', 'error_code' => '401'])
                ->status(400);
        }

        $file = $request->file($requestFileName);

        $version = $request->get('version');
        $description = $request->get('description');
        $file->storeAs(SoftwareClient::$storageLocation, 'client-'. $version . '.zip');

        $softwareClient = SoftwareClient::create(['version' => $version, 'description' => $description]);

        return response($softwareClient);
    }

    public function destroy(Request $request, $id)
    {
        $software = SoftwareClient::find($id);

        Storage::delete(SoftwareClient::$storageLocation . $software->version);

        $software->delete();
    }
}
