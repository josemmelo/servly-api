<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Servly\Models\Log;

class LogsController extends Controller
{
    public function index(Request $request){
        if(! $request->has('date'))
            return Log::orderBy('created_at', 'DESC')->paginate(20);

        return Log::where('created_at', 'LIKE', '%'. $request->get('date'). '%')
            ->orderBy('created_at', 'DESC')->paginate(20);
    }
}
