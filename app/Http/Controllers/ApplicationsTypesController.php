<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Servly\Models\ApplicationType;

class ApplicationsTypesController extends Controller
{
    public function store(Request $request){
        $app = [
            'type' => $request->get('type'),
            'description' => $request->get('description')
        ];
        try{
            $applicationType = ApplicationType::create($app);

            return response()->json($applicationType, 201);
        }catch (\Exception $e){
            return response()->json($e->getMessage(), 400);
        }
    }

    public function update(Request $request, $typeId) {
        try{
            ApplicationType::where('id', $typeId)->update($request->all());
            return response()->json([], 200);
        }catch (\Exception $e){
            return response()->json($e->getMessage(), 400);
        }
    }

    public function destroy(Request $request, $typeId) {
        ApplicationType::where('id', $typeId)->delete();
    }

    public function index() {
        return ApplicationType::all();
    }

}
