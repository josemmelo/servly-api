<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;


class SubscriptionController extends Controller
{
    public static $SUBSCRIPTION_PLAN_NAME = 'SERVLY_PLAN';

    public function subscribe_process(Request $request)
    {
        try {
            Stripe::setApiKey(env('STRIPE_SECRET'));

            $user = $request->user();

            $subscriptionId = strtolower($request->get('subscriptionName'));
            $stripeToken = $request->get('stripeToken');

            if ($user->subscribed(SubscriptionController::$SUBSCRIPTION_PLAN_NAME)) {
                $user->subscription(SubscriptionController::$SUBSCRIPTION_PLAN_NAME)->swap($subscriptionId);
            } else {
                $user->newSubscription(SubscriptionController::$SUBSCRIPTION_PLAN_NAME, $subscriptionId)
                    ->create($stripeToken);
            }

            return response()->json(['message' => 'Subscription successful, you get the course!']);
        } catch (\Exception $ex) {
            return response()->json(['message' => $ex->getMessage()]);
        }
    }
}
