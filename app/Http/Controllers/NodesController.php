<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Servly\Helpers\NodesHelper;

class NodesController extends Controller
{
    public function index(Request $request)
    {
        return $request->user()->nodes()
            ->with('applications')
            ->with('monitored_nodes')
            ->get();
    }

    public function update(Request $request, $nodeId)
    {
        return NodesHelper::update($request, $nodeId);
    }

    public function store(Request $request)
    {
        throw new \Exception();
        return NodesHelper::store($request->user(), $request->all());
    }

    public function show(Request $request, $nodeId)
    {
        return NodesHelper::show($request->user(), $nodeId);
    }

    public function destroy(Request $request, $nodeId)
    {
        return NodesHelper::delete($request->user(), $nodeId);
    }
}
