<?php

namespace Servly\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Servly\Helpers\ApplicationsHelper;
use Servly\Helpers\RequestValidatorHelper;
use Servly\Helpers\UsersHelper;
use Servly\Models\ApplicationType;
use Servly\Models\Log;
use Servly\Models\Application;

class ApplicationsController extends Controller
{
    public function index(Request $request, $nodeId = null){
        return ApplicationsHelper::index($request->user()->id, $nodeId);
    }

    public function store(Request $request, $nodeId){
        return ApplicationsHelper::store($request->all(), $nodeId);
    }

    public function update(Request $request, $id)
    {
        return ApplicationsHelper::update($request, $id);
    }

    public function show(Request $request, $applicationId) {
        return ApplicationsHelper::show($request->user()->id, $applicationId);
    }

    public function information(Request $request, $appId) {
        if( ! UsersHelper::authorize($request->header('clientid'), $request->header('clientsecret')) )
            return error_response('Client authorization failed.', 401);

        return ApplicationsHelper::information($appId);
    }

    public function destroy($id) {
        DB::transaction(function () use ($id) {
            $app = Application::find($id);
            $app->application_info()->delete();
            $app->application_filters()->delete();
            $app->delete();
        });
    }
}
