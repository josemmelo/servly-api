<?php

namespace Servly\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Servly\Helpers\ApplicationsHelper;
use Servly\Jobs\SaveApplicationLog;

class ApplicationLogController extends Controller
{
    public function store(Request $request, $applicationId)
    {
        $time = str_replace(",", ".", $request->get('time'));

        $msg = $request->get('msg');

        if (empty($msg))
            $msg = "n/a";

        $applicationLog = [
            'application_id' => $applicationId,
            'app'            => $request->get('app'),
            'log_type'       => $request->get('type'),
            'message'        => $msg,
            'class'          => $request->get('class'),
            'date'           => Carbon::parse($request->get('date')),
            'time'           => Carbon::parse($time),
        ];

        SaveApplicationLog::dispatch($applicationLog);
    }

    public function index(Request $request, $applicationId = null) {
        return ApplicationsHelper::getLogs($request->user()->id, $applicationId, $request->get('q'));
    }

    public function stats(Request $request, $applicationId = null) {
        return ApplicationsHelper::getLogsStats($request->user()->id, $applicationId);

    }
}
