<?php

namespace Servly\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Servly\Models\Log;

class LogHttpRequests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request, $response, $elapsedTime;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestInfo, $response, $elapsedTime)
    {
        $this->request = $requestInfo;
        $this->response = $response;
        $this->elapsedTime = $elapsedTime;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $logContent = [
            'method'          => $this->request['method'],
            'headers'         => $this->request['headers'],
            'parameters'      => $this->request['parameters'],
            'url'             => $this->request['url'],
            'user_agent'      => $this->request['user_agent'],
            'user_ip_address' => $this->request['user_ip_address'],
            'user'            => $this->request['user'],
            'response'        => $this->response,
            'elapsed_time'    => $this->elapsedTime,
        ];

        Log::create([
                        'content' => json_encode($logContent),
                    ]);
    }
}
