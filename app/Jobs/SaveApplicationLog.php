<?php

namespace Servly\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Servly\Models\ApplicationLog;
use Servly\Models\ApplicationLogFilters;
use Symfony\Component\Debug\Debug;

class SaveApplicationLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $applicationLog;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($applicationLog)
    {
        $this->applicationLog = $applicationLog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filters = ApplicationLogFilters::where('application_id', $this->applicationLog['application_id'])
            ->first();

        if ( ! in_array($this->applicationLog['log_type'], $filters->filters->log_level))
            return response('Log Type is not allowed for application.');

        ApplicationLog::firstOrCreate($this->applicationLog);
    }
}
