<?php

namespace Servly\Models;


use Illuminate\Database\Eloquent\Model;

class ApplicationLogFilters extends Model
{
    protected $table = 'application_log_filters';
    public $timestamps = false;

    protected $hidden = ['id', 'application_id'];

    protected $fillable = ['filters'];

    public static $defaultFilters = [
        'log_level' => [
            'WARN', 'ERROR'
        ]
    ];

    public function getFiltersAttribute($value){
        return json_decode($value);
    }
}