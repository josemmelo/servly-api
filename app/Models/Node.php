<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $table    = 'nodes';
    protected $fillable = ['info', 'name', 'ip', 'os', 'machine_name', 'type', 'errors', 'up_time', 'cpu_max_usage', 'memory_max_usage', 'cpu_notify', 'memory_notify'];

    public function user()
    {
        return $this->belongsTo('Servly\Models\User');
    }

    public function applications()
    {
        return $this->hasMany('Servly\Models\Application');
    }

    public function monitored_nodes(){
        return $this->hasMany('Servly\Models\MonitoredNode');
    }
}
