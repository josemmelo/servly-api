<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public static $requiredParamsToCreate = [
        'technology', 'name', 'type', 'regex', 'active_services'
    ];
    public        $timestamps             = false;
    protected     $table                  = "applications";
    protected     $fillable               = [
        'up_time', 'technology', 'name', 'type_id', 'node_id', 'regex', 'active_services',
    ];

    public function application_info()
    {
        return $this->hasOne('Servly\Models\ApplicationInfo');
    }

    public function type()
    {
        return $this->belongsTo('Servly\Models\ApplicationType');
    }

    public function node()
    {
        return $this->belongsTo('Servly\Models\Node');
    }

    public function application_filters()
    {
        return $this->hasOne('Servly\Models\ApplicationLogFilters', 'application_id', 'id');
    }
}
