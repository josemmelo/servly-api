<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvailableServices extends Model
{
    use SoftDeletes;

    protected $table    = 'available_services';
    protected $fillable = ['name', 'identifier', 'description', 'subscription_type'];

    protected $dates = ['deleted_at'];
}
