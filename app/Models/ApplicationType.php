<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationType extends Model
{
    use SoftDeletes;

    protected $table = 'application_types';
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    protected $fillable = ['type', 'description'];

    public function applications() {
        return $this->hasMany('Servly\Models\Application', 'type_id','id');
    }
}
