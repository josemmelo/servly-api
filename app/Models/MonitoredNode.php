<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class MonitoredNode extends Model
{
    protected $table = 'monitored_nodes';
    protected     $fillable               = [
        'client_id', 'client_secret', 'app_id', 'node_id', 'socketId'
    ];

    public function application()
    {
        return $this->belongsTo('Servly\Models\Application');
    }

    public function node()
    {
        return $this->belongsTo('Servly\Models\Node');
    }

}
