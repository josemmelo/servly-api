<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationInfo extends Model
{
    protected $table = 'application_info';
    protected $hidden = ['id', 'application_id'];

    public $timestamps = false;
    protected $fillable = ['total_requests', 'total_errors', 'information'];


    public function application(){
        return $this->belongsTo('Servly\Models\Application', 'application_id', 'id');
    }

    public function getInformationAttribute($value) {
        return json_decode($value);
    }
}
