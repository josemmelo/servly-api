<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class DataProcessor extends Model
{
    protected $table = 'data_processors';

    public $timestamps = false;
}
