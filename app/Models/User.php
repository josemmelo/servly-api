<?php

namespace Servly\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'client_id', 'client_secret',
    ];

    public static $requiredParamsToCreate = [
        'name', 'email', 'password'
    ];

    protected $casts = [
        'is_admin' => 'boolean',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'stripe_id'
    ];

    public function nodes(){
        return $this->hasMany('Servly\Models\Node');
    }
}
