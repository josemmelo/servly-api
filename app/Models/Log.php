<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $fillable = [
        'content'
    ];

}
