<?php

namespace Servly\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationLog extends Model
{
    protected $table = 'application_logs';
    public $timestamps = false;

    protected $fillable = [
        'application_id', 'app', 'log_type', 'message', 'class', 'date', 'time'
    ];

    public function application(){
        return $this->belongsTo('Servly\Models\Application');
    }
}
