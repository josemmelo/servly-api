<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Servly\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'client_id'      => $faker->unique()->uuid,
        'client_secret'  => $faker->unique()->uuid,
        'remember_token' => str_random(10),
    ];
});

$factory->define(Servly\Models\Node::class, function (Faker $faker) {

    return [
        'name'             => $faker->name,
        'ip'               => $faker->ipv4,
        'machine_name'     => $faker->domainName,
        'os'               => $faker->randomElement(['ubuntu', 'windows', 'centos', 'mac']),
        'cpu_max_usage'    => $faker->randomFloat(3),
        'memory_max_usage' => $faker->randomNumber(7),
        'up_time'          => $faker->randomNumber(7),

    ];
});

$factory->define(Servly\Models\Application::class, function (Faker $faker) {
    return [
        'name'            => $faker->name,
        'up_time'         => $faker->randomNumber(7),
        'technology'      => $faker->name,
        'active_services' => $faker->randomElement(['["mulesoft_apps_domains", "node_monitor"]', '["node_monitor"]']),
        'regex'           => "(?P<Date>[0-9-]+)([ ]+)(?P<Time>[0-9:,]+)([ ]+)(?P<App>[[a-zA-Z0-9_\-\[,\].]+])([ ]+)(?P<Type>[A-Z]+)([ ]+)(?P<Class>[a-zA-Z.]+)([ ]+)-([ ]*)(?P<Msg>[a-zA-Z0-9_\-\[,\]./\n\r\t\*:@\(\-\)\'= ]+)",
        'type_id'         => 1,
    ];
});

