<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->integer('node_id')->unsigned();
            $table->integer('type_id')->unsigned();

            $table->bigInteger('up_time')->default(0);
            $table->string('technology');
            $table->boolean('notifications_enabled')->default(false);

            $table->foreign('node_id')->references('id')->on('nodes');
            $table->foreign('type_id')->references('id')->on('application_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
