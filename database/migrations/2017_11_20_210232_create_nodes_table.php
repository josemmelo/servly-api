<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name');
            $table->string('ip');
            $table->string('machine_name');
            $table->string('os')->default('');


            $table->integer('errors')->default(0);
            $table->bigInteger('up_time')->default(0);

            $table->double('cpu_max_usage')->default(0);
            $table->bigInteger('memory_max_usage')->default(0);

            $table->integer('cpu_notify')->default(-1);
            $table->integer('memory_notify')->default(-1);

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
