<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataProcessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_processors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('machine_platform');
            $table->string('agent');
            $table->string('ip');
            $table->bigInteger('pid');
            $table->string('listening_port');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_processors');
    }
}
