<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoredNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitored_nodes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('socketId')->nullable();
            $table->string('client_id');
            $table->string('client_secret');
            $table->integer('node_id')->unsigned();
            $table->integer('app_id')->unsigned();

            $table->foreign('node_id')->references('id')->on('nodes');
            $table->foreign('app_id')->references('id')->on('applications');
            $table->foreign('client_id')->references('client_id')->on('users');
            $table->foreign('client_secret')->references('client_secret')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitored_nodes');
    }
}
