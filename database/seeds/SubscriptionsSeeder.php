
<?php

use Illuminate\Database\Seeder;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Servly\Models\SubscriptionPlan::create([
                                                    'plan_name'        => 'free',
                                                    'pricing' => 0,
                                                    'features' => json_encode(['servers' => 2,
                                                                   'applications' => 5,
                                                                   'realtime' => false,
                                                                   'notification' => false])
                                                ]);

        \Servly\Models\SubscriptionPlan::create([
                                                    'plan_name'        => 'silver',
                                                    'pricing' => 10,
                                                    'features' => json_encode(['servers' => 10,
                                                                   'applications' => 40,
                                                                   'realtime' => true,
                                                                   'notification' =>true])
                                                ]);

        \Servly\Models\SubscriptionPlan::create([
                                                    'plan_name'        => 'premium',
                                                    'pricing' => 20,
                                                    'features' => json_encode(['servers' => -1,'applications' => -1,
                                                                   'realtime' => true, 'notification' =>true])
                                                ]);

    }
}