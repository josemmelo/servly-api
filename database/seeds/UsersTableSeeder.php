<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Servly\Models\ApplicationType::create([
                                                   'type'        => 'Mulesoft',
                                                   'description' => 'Mule runtime',
                                               ]);

        /*\Servly\Models\DataProcessor::create([
                                                 'name'      => 'Python log processor',
                                                 'token'     => '4F13D333-5759-4D6E-8BD1-BA8318938016',
                                                 'agent'     => 'python-requests\/2.18.4',
                                                 'ip'        => '10.0.2.2',
                                                 'is_active' => true,
                                             ]);
*/
        factory(\Servly\Models\User::class, 50)->create()->each(function ($u) {
            $node = $u->nodes()->save(factory(\Servly\Models\Node::class)->make());
            $application = $node->applications()->save(factory(\Servly\Models\Application::class)->make());
            $application->application_info()->create();
            $application->application_filters()->create([
                                                            'filters' => '{"log_level": ["WARN", "ERROR"]}',
                                                        ]);

            $node = $u->nodes()->save(factory(\Servly\Models\Node::class)->make());
            $application = $node->applications()->save(factory(\Servly\Models\Application::class)->make());
            $application->application_info()->create();
            $application->application_filters()->create([
                                                            'filters' => '{"log_level": ["WARN", "ERROR"]}',
                                                        ]);
        });
    }
}
