<?php

use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Servly\Models\ApplicationType::create([
                                                   'type'        => 'Mulesoft',
                                                   'description' => 'Mule runtime',
                                                   'regex'       => "(?P<Date>[0-9-]+)([ ]+)(?P<Time>[0-9:,]+)([ ]+)(?P<App>[[a-zA-Z0-9_\\-\\[,\\].]+])([ ]+)(?P<Type>[A-Z]+)([ ]+)(?P<Class>[a-zA-Z.]+)([ ]+)-([ ]*)(?P<Msg>[a-zA-Z0-9_\\-\\[,\\]./\\n\\r\\t\\*:@\\(\\-\\)\\\'= ]+)",
                                               ]);


        \Servly\Models\User::create(
            [
                'name'          => 'Admin',
                'email'         => 'admin@servly.com',
                'password'      => \Illuminate\Support\Facades\Hash::make('password'),
                'client_id'     => \Ramsey\Uuid\Uuid::uuid4(),
                'client_secret' => \Ramsey\Uuid\Uuid::uuid4(),
                'is_admin'      => true,
            ]
        );

        //TODO: client id and client secret!!

        \Servly\Models\AvailableServices::create(
            [
                'name'        => 'Mulesoft Apps and Domains monitor',
                'identifier'  => 'mulesoft_apps_domains',
                'description' => 'Monitor mulesoft application and domains (logs, number of running applications/domains,...)'
            ]);
        \Servly\Models\AvailableServices::create(
            [
                'name'        => 'Node stats monitor',
                'identifier'  => 'node_monitor',
                'description' => 'Monitor node system information (CPU, Memory, ...)',
            ]);
        \Servly\Models\AvailableServices::create(
            [
                'name'        => 'Logs Monitor',
                'identifier'  => 'logs_monitor',
                'description' => 'Monitor application logs',
            ]);
    }
}
