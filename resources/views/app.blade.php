<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>{{ env('APP_NAME') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/app.css')}}" rel="stylesheet"/>

    <script src="https://js.stripe.com/v2/"></script>
    <script>
        window.address = '{{ env('APP_URL') }}';
        window.Stripe = Stripe;
        window.StripeKey = '{{env('STRIPE_KEY')}}';
        window.Stripe.setPublishableKey(window.StripeKey);
    </script>
</head>
<body>

<div class="wrapper" id="app">
    <side-bar v-if="$auth.getUser() != null"></side-bar>

    <div class="main-panel" v-if="$auth.getUser() != null">
        <nav-bar></nav-bar>

        <div class="content">
            <router-view></router-view>
        </div>

        <footer-nav></footer-nav>
    </div>


    <router-view  v-if="$auth.getUser() == null"></router-view>

</div>


</body>

<script src="{{asset('js/app.js')}}"></script>

</html>
