import Vue from 'vue';

export default {

    formatBytes: function (numBytes, b) {
        var obj = this.formatBytesAsObject(numBytes, b);
        return obj.value + " " + obj.un;
    },

    formatBytesAsObject: function (numBytes, b) {
        if (0 == numBytes) return {"value": 0, "un": "Bytes"};

        var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
            f = Math.floor(Math.log(numBytes) / Math.log(c));

        return {"value": parseFloat((numBytes / Math.pow(c, f)).toFixed(d)), "un": e[f]};
    },

    currentTimeString: function () {
        var currentDate = new Date();
        return currentDate.getHours() + ":"
            + currentDate.getMinutes() + ":" + currentDate.getSeconds();
    },

    goToPage: function (router, pageName, params) {
        router.push({name: pageName, params: params});
    },
    getUserIP: function (onNewIP) {
        Vue.http.get('https://api.ipify.org?format=json').then((resp) => {
            onNewIP(resp.body.ip);
        })
    },

    toHHMMSS : function (number) {
        var sec_num = parseInt(number, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    },



    array: {
        unique : function (array) {
            var a = array.concat();
            for (var i = 0; i < a.length; ++i) {
                for (var j = i + 1; j < a.length; ++j) {
                    if (a[i] === a[j])
                        a.splice(j--, 1);
                }
            }

            return a;
        },

        mergeWithoutDuplicates : function (firstArray, secondArray) {
            firstArray = firstArray.concat(secondArray);

            return this.unique(firstArray);
        }
    }
}