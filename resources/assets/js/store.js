import usersService from './services/users'
import nodesService from './services/nodes'
import auth from './auth'

export default {
    store: {
        user: function() {
            return auth.getUser();
        },
        nodes: {
            list: [],
            maxCpuCalculatedUsage: 0,
            operativeSystemsList: [],
        },
        applications:{
            list: []
        },
        notifications:[]
    },
    utils:{
        getBy : function (filterName, list, value) {
            var result = null;
            list.forEach(n => {
                if (n[filterName] == value) {
                    result = n;
                    return;
                }
            });
            return result;
        },

        getMax(list, param) {
            var max = 0;
            list.forEach((value) => {
                if(value[param] > max) max = value[param];
            });
            return max;
        },

        getAllValuesForField(list, field) {
            var values = [];
            list.forEach((value) => {
                if(values.indexOf(value[field]) === -1) values.push(value[field]);
            });
            return values;
        }
    }
}