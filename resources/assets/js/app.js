/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./libs/bootstrap');
require('./libs/bootstrap-notify');
require('./libs/bootstrap-select');
require('./libs/chartist.min');
require('./libs/light-bootstrap-dashboard');

var io = require('./libs/socket.io');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

Vue.use(VueRouter);
Vue.use(VueResource);

import store from './store'
import routes from './routes'
import utils from './utils'
import auth from './auth'
import configs from './globals/config'
import urls from './globals/urls'

import NavBar from './modules/user/core-modules/NavBar'
import SideBar from './modules/user/core-modules/SideBar'
import Footer from './modules/user/core-modules/Footer'


Vue.prototype.$appName = 'Servly';
Vue.prototype.$store = store;
Vue.prototype.$utils = utils;
Vue.prototype.$configs = configs;
Vue.prototype.$auth = auth;
Vue.prototype.$urls = urls;

import NodeService from './services/nodes'
import UsersService from './services/users'
import ApplicationsService from './services/applications'
import LogsService from './services/logs'
import ApplicationTypesService from './services/application_types'
import AvailableServicesService from './services/available_services'
import ClientSoftwareService from './services/client_software'
import SubscriptionsService from './services/subscriptions'


Vue.prototype.$services = {
    'nodes': NodeService,
    'users': UsersService,
    'applications': ApplicationsService,
    'logs': LogsService,
    'application_types': ApplicationTypesService,
    'available_services': AvailableServicesService,
    'client_software' : ClientSoftwareService,
    'subscriptions' : SubscriptionsService
}

Vue.prototype.$io = io;

var socket = io(window.address + configs.SOCKET_PORT);
socket.on('connect', () => {
    console.log("Connected to server!");
    utils.getUserIP(function (ip) {
        console.log("Connected to server!");
        var client_id = (store.store.user == null) ? null : store.store.user.client_id;

        socket.emit('client_register', {
            'client_id': client_id,
            'ip': ip,
            'user_agent': navigator.userAgent
        });
    });
});

socket.on('disconnect', function () {
});

Vue.prototype.$socketIO = socket;


const router = new VueRouter({mode: 'history', routes: routes.routes()});

new Vue({
    router,
    components: {
        NavBar,
        SideBar,
        'footer-nav': Footer
    }
}).$mount('#app');

