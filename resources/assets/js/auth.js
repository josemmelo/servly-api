import urls from './globals/urls';
import oauth from './globals/oauth';
import userService from './services/users';
import Vue from 'vue';

export default {
    login(email, pass, cb) {
        var postBody = {
            'client_id': oauth.CLIENT_ID,
            'client_secret': oauth.CLIENT_SECRET,
            'grant_type': oauth.GRANT_TYPE,
            'username': email,
            'password': pass
        };

        Vue.http.post(urls.OAUTH_TOKEN, postBody).then(response => {

            localStorage.setItem("token", response.body.access_token);
            var expirationTime = response.body.expires_in + (new Date().getTime());
            localStorage.setItem("expiration_time", expirationTime);
            userService.updateLocalMe(() => {
                cb(response);
            });


        }, response => {
            cb(response);
        });
    },

    getToken() {
        return localStorage.token
    },

    logout(cb) {
        delete localStorage.token
        delete localStorage.expiration_time
        delete localStorage.user
        if (cb) cb()
        this.onChange(false)
    },

    loggedIn() {
        if (localStorage.expiration_time <= (new Date().getTime())) {
            this.logout();
            return false;
        }
        return !!localStorage.token
    },

    onChange() {
    },

    register(user, cb) {
        userService.create(user, cb);
    },

    getUser() {
        if(!localStorage.user)
            return null;

        return JSON.parse(localStorage.user);
    }

}

function pretendRequest(email, pass, cb) {
    setTimeout(() => {
        if (email === 'joe@example.com' && pass === 'password1') {
            cb({
                authenticated: true,
                token: Math.random().toString(36).substring(7)
            })
        } else {
            cb({authenticated: false})
        }
    }, 0)
}