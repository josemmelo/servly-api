import urls from '../globals/urls';
import apiRequest from './api_request';

export default {

    post(formData, cb) {
        apiRequest.post(urls.SUBSCRIPTIONS.SUBSCRIBE,formData, [], (response) => {
            cb(response.body);
        });
    }
}