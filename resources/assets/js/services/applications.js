import urls from '../globals/urls';
import apiRequest from './api_request';

export default {

    list(cb) {
        apiRequest.get(urls.APPLICATIONS.LIST, [], (response) => {
            cb(response.body);
        });
    },

    get(id, cb) {
        apiRequest.get(urls.APPLICATIONS.GET.replace(":id", id), [], (response) => {
            cb(response.body);
        });
    },

    getLogs(appId, page, query, cb) {
        apiRequest.get(urls.APPLICATIONS.GET_LOGS.replace(":id", appId), {page: page, q:query}, (response) => {
            cb(response.body);
        });
    },

    getLogsStats(appId, cb) {
        apiRequest.get(urls.APPLICATIONS.GET_LOGS_STATS.replace(":id", appId), [], (response) => {
            cb(response.body);
        });
    },

    save(appId, application, cb) {
        apiRequest.put(urls.APPLICATIONS.UPDATE.replace(":id", appId), application, (response) => {
            cb(response.body);
        });
    },

    create(nodeId, application, cb) {
        apiRequest.post(urls.APPLICATIONS.STORE.replace(":id", nodeId), application, [], (response) => {
            cb(response);
        });
    },

    delete(appId, cb) {
        apiRequest.delete(urls.APPLICATIONS.DELETE.replace(":id", appId), (response) => {
            cb(response.body);
        });
    }

}