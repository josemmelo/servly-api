import urls from '../globals/urls';
import apiRequest from './api_request';

export default {

    list(cb) {
        apiRequest.get(urls.LOGS.LIST, [], (response) => {
            cb(response.body);
        });
    },

    stats(cb) {
        apiRequest.get(urls.LOGS.STATS, [], (response) => {
            cb(response.body);
        });
    }
}