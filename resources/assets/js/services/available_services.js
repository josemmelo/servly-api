import urls from '../globals/urls';
import apiRequest from './api_request';

export default {

    list(cb) {
        apiRequest.get(urls.AVAILABLE_SERVICES.LIST, [], (response) => {
            cb(response.body);
        });
    }
}