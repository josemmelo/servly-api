import Vue from 'vue';
import auth from '../auth';


export default {
    get(url, params, cb) {
        var headers = {
            'Authorization': 'Bearer ' + auth.getToken()
        };

        Vue.http.get(url, {params:params , headers: headers} ).then(response => {
            cb(response);
        }, response => {
            cb(response);
        });
    },

    post (url, body, params, cb) {
        var headers = {
            'Authorization': 'Bearer ' + auth.getToken()
        };

        Vue.http.post(url, body, {params:params , headers: headers} ).then(response => {
            cb(response);
        }, response => {
            cb(response);
        });

    },

    put (url, params, cb) {
        var headers = {
            'Authorization': 'Bearer ' + auth.getToken()
        };

        Vue.http.put(url, params , {headers:headers}).then(response => {
            cb(response);
        }, response => {
            cb(response);
        });

    },

    delete (url, cb) {
        var headers = {
            'Authorization': 'Bearer ' + auth.getToken()
        };

        Vue.http.delete(url, {headers:headers}).then(response => {
            cb(response);
        }, response => {
            cb(response);
        });
    }
}