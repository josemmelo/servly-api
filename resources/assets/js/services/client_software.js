import urls from '../globals/urls';
import apiRequest from './api_request';

export default {

    post(formData, cb) {
        apiRequest.post(urls.CLIENTS_SOFTWARE.STORE,formData, [], (response) => {
            cb(response.body);
        });
    },

    list(cb) {
        apiRequest.get(urls.CLIENTS_SOFTWARE.LIST, [], (response) => {
            cb(response.body);
        });
    },

    download(cb) {
        apiRequest.get(urls.CLIENTS_SOFTWARE.DOWNLOAD, [], (response) => {
            cb(response.body);
        });
    }
}