import urls from '../globals/urls';
import apiRequest from './api_request';


export default {
    me(cb) {
        if(localStorage.user == null)
        {
            this.updateLocalMe(cb)
        }
        else {
            cb(JSON.parse(localStorage.user));
        }
    },

    updateLocalMe(cb) {
        if(localStorage.token == null)
            return;

        apiRequest.get(urls.USERS.ME, [], (response) => {
            localStorage.user = JSON.stringify(response.body);
            cb(localStorage.user);
        });
    },

    update(params, cb) {
        apiRequest.put(urls.USERS.UPDATE, params, cb);
    },

    create(user, cb) {
        apiRequest.post(urls.USERS.CREATE, user,null, cb);
    },

    list(params ,cb) {
        apiRequest.get(urls.USERS.LIST, params, (response) => {
            cb(response.body);
        });
    },
    active(cb) {
        apiRequest.get(urls.USERS.ACTIVE, [], (response) => {
            cb(response.body);
        });

    }

}