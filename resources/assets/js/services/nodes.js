import urls from '../globals/urls';
import apiRequest from './api_request';

export default {
    list(cb) {
        apiRequest.get(urls.NODES.LIST, [], (response) => {
            cb(response.body);
        });
    },

    get(id, cb) {
        apiRequest.get(urls.NODES.GET.replace(":id", id), [], (response) => {
            cb(response.body);
        });
    },

    save(id, node, cb) {
        apiRequest.put(urls.NODES.PUT.replace(":id", id), node, (response) => {
            cb(response.body);
        });
    },

    create(node, cb) {
        apiRequest.post(urls.NODES.POST, node, [], (response) => {
            cb(response);
        });
    },


    _moreInfo: function(node, append) {
        return node.monitored_nodes.length + '/' + node.applications.length + ' apps being monitored. ' + append;
    },
    status(node) {
        var status = {
            fa_class: 'fa fa-circle text-success',
            msg: 'Ok',
            moreInfo: this._moreInfo(node, ''),
            showingMoreInfo : false,
            toggleShowingMoreInfo : function(){
                this.showingMoreInfo = ! this.showingMoreInfo
            }
        }

        if(node.monitored_nodes.length == 0 && node.applications.length > 0)
        {
            status.fa_class = 'fa fa-circle text-danger';
            status.msg = 'Whoops!';
            status.moreInfo = this._moreInfo(node, 'Live information not available.');
        }
        else if(node.monitored_nodes.length < node.applications.length)
        {
            status.fa_class = 'fa fa-circle text-warning';
            status.msg = 'Whoops!';
        }
        return status;
    }
}