import LoginPage from './modules/guest/LoginPage.vue'
import FrontPage from './modules/guest/FrontPage.vue'
import RegisterPage from './modules/guest/RegisterPage.vue'

import DashboardPage from './modules/user/pages/DashboardPage.vue'
import NodesMonitorPage from './modules/user/pages/NodesMonitorPage'
import NodePage from './modules/user/pages/NodePage'
import NotFoundPage from './modules/user/pages/NotFoundPage'
import ApplicationPage from './modules/user/pages/ApplicationPage'
import ApplicationLogsPage from './modules/user/pages/ApplicationLogsPage'
import AccountPage from './modules/user/pages/AccountPage'
import AddNodePage from './modules/user/pages/AddNodePage'
import AddApplicationPage from './modules/user/pages/AddApplicationPage'
import DownloadClientPage from './modules/user/pages/DownloadClientPage'


import UsersPage from './modules/user/admin/pages/UsersPage'
import ActiveServicesPage from './modules/user/admin/pages/ActiveServicesPage'
import ApplicationTypesPage from './modules/user/admin/pages/ApplicationTypesPage'
import ClientSoftwarePage from './modules/user/admin/pages/ClientSoftwarePage'

import auth from './auth'

export default {

    _requireAuth : function(to, from, next) {
        if (!auth.loggedIn()) {
            next({
                path: '/login'
            })
        } else {
            next()
        }
    },

    _requireAdminAuth : function(to, from, next) {
        console.log(auth.getUser())
        if (!auth.loggedIn() || ! auth.getUser().is_admin) {
            next({
                path: '/login'
            })
        } else {
            next()
        }
    },


    _redirectIfAuth : function(to, from, next) {
        if (auth.loggedIn()) {
            next({
                path: '/dashboard'
            })
        } else {
            next()
        }
    },

    routes : function() {
        var routesList = [
            {
                name: 'login',
                path: '/login',
                component: LoginPage,
                beforeEnter: this._redirectIfAuth
            },
            {
                name: 'register',
                path: '/register',
                component: RegisterPage,
                beforeEnter: this._redirectIfAuth
            },

            {
                name: 'front',
                path: '/',
                component: FrontPage,
                beforeEnter: this._redirectIfAuth
            },
            {
                name: 'dashboard',
                path: '/dashboard',
                component: DashboardPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'base',
                path: '/',
                component: DashboardPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'nodes',
                path: '/nodes',
                component: NodesMonitorPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'node',
                path: '/nodes/:id',
                component: NodePage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'add_node',
                path: '/nodes/add',
                component: AddNodePage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'application',
                path: '/application/:id',
                component: ApplicationPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'add_application',
                path: '/nodes/:id/applications/add',
                component: AddApplicationPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'application_logs',
                path: '/application/:id/logs',
                component: ApplicationLogsPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'account',
                path: '/account',
                component: AccountPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'download_client',
                path: '/download-client',
                component: DownloadClientPage,
                beforeEnter: this._requireAuth
            },
            {
                name: 'not_found',
                path: '/:other',
                component: NotFoundPage
            }
        ];

        if(auth.getUser() != null && auth.getUser().is_admin)
        {
            routesList.push({
                name: 'users',
                path: '/admin/users',
                component: UsersPage,
                beforeEnter: this._requireAdminAuth
            });
            routesList.push({
                name: 'active_services',
                path: '/admin/active_services',
                component: ActiveServicesPage,
                beforeEnter: this._requireAdminAuth
            });
            routesList.push({
                name: 'client_software',
                path: '/admin/client_software',
                component: ClientSoftwarePage,
                beforeEnter: this._requireAdminAuth
            });
            routesList.push({
                name: 'application_types',
                path: '/admin/application_types',
                component: ApplicationTypesPage,
                beforeEnter: this._requireAdminAuth
            });


        };

        return routesList
    }
}