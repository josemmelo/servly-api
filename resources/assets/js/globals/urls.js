export default {
    OAUTH_TOKEN : '/oauth/token',

    USERS: {
        ME : '/api/users/me',
        UPDATE : '/api/users',
        CREATE: '/api/users',
        LIST: '/api/admin/users',
        ACTIVE: '/api/admin/users/active'
    },

    NODES: {
        LIST : '/api/nodes',
        GET : '/api/nodes/:id',
        PUT : '/api/nodes/:id',
        POST : '/api/nodes'
    },

    APPLICATIONS: {
        LIST : '/api/applications',
        GET : '/api/applications/:id',
        UPDATE: '/api/applications/:id',
        GET_LOGS: '/api/applications/:id/logs',
        GET_LOGS_STATS: '/api/applications/:id/logs/stats',
        STORE: '/api/nodes/:id/applications',
        DELETE: '/api/applications/:id'
    },

    LOGS: {
        LIST : '/api/logs',
        STATS : '/api/logs/stats'
    },

    APPLICATION_TYPES: {
        LIST: '/api/application_types'
    },

    AVAILABLE_SERVICES: {
        LIST: '/api/available_services'
    },

    CLIENTS_SOFTWARE : {
        STORE: '/api/admin/software_client',
        LIST: '/api/software_client',
        DOWNLOAD : '/api/software_client/download'
    },

    SUBSCRIPTIONS : {
        SUBSCRIBE : '/api/subscriptions/subscribe'
    }
}