export default {
    SOCKET_PORT : ':3001',
    SOCKET_CHANNELS : {
        OS_MONITOR : function(user, node) {
            return 'os_monitor_s-' + user.client_id + '-id' + node;
        },
        LOG_MONITOR : function(user, app) {
            return 'logs_monitor_s-' + user.client_id + '-id' + app;
        },
    },

    CHARTS: {
        COLORS:{
            GREEN : 'rgba(42, 178, 123,0.6)',
            BLUE: 'rgba(67, 160, 214, 0.7)',
            ORANGE : 'rgba(191, 83, 41, 0.6)',
        },

        COLOR_LIST : function() {
            return [this.COLORS.BLUE, this.COLORS.GREEN, this.COLORS.ORANGE]
        },
        MAX_SHOWING : 0.5*60
    }
}