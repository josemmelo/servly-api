var mysqlConnection = require('../database/mysql');
var ip = require('ip');

module.exports = function() {
    var address = ip.address();
    
    var registerDataProcessor = "replace into data_processors (name, machine_platform, agent, ip, pid, listening_port)";
    registerDataProcessor += " values (?,?,?,?,?,?)";
    
    var values =  [process.env.APP_NAME, process.platform, process.title, address, process.pid, process.env.LISTENING_PORT];

    mysqlConnection.conn.query(registerDataProcessor, values, (error, results, fields) => {
    });
    
}