let OS_MONITOR_CHANNEL = 'os_monitor_s';
let LOGS_MONITOR_CHANNEL = 'logs_monitor_s';

let Node = require('../../models/Node');
let ApplicationLog = require('../../models/ApplicationLog');
var emitSocket = {};
var nodeInfoStorage = {};
var storeLogDatabase = [];

function getChannelName(type, clientId, id) {
    return type+'-'+clientId+'-id'+id;
}

var ApplicationMessagesHandler = function(ch, socketIO){
    sendSocket();

    updateDB();

    function sendSocket(){
        let toEmit = Object.assign({},emitSocket);
        emitSocket = {};
        
        Object.keys(toEmit).forEach((toChannel) => {
            storeLogDatabase.push(Object.values(toEmit[toChannel]));
            socketIO.emit(toChannel, toEmit[toChannel]);
        });

        setTimeout(sendSocket, 1000);
    }

    function updateDB() {
        Object.keys(nodeInfoStorage).forEach((nodeId) => {
            Node.select(nodeId, (error, results, fields) => {
                console.log("GET NODE INFO TO UPDATE - " + nodeId);
                if (error) throw error;
                
                var updateMemory = results[0].memory_max_usage < nodeInfo.memory_max_usage;            
                var updateCpu = results[0].cpu_max_usage < nodeInfo.cpu_max_usage;
                
                if(! updateMemory && ! updateCpu)  return;

                nodeInfo.memory_max_usage = updateMemory ? nodeInfo.memory_max_usage : results[0].memory_max_usage;
                nodeInfo.cpu_max_usage = updateCpu ? nodeInfo.cpu_max_usage : results[0].cpu_max_usage;

                nodeInfoStorage[nodeId] = nodeInfo;

                Node.update(nodeId, nodeInfo, (error, results, fields) => {
                    console.log("UPDATE NODE INFO - " + nodeId);
                    if (error) throw error;
                });
            });
        });   

        ApplicationLog.bulkInsert(storeLogDatabase, () => {

        });
        storeLogDatabase = [];

        setTimeout(updateDB, 10000);
    }

    function _OSMonitor(messageAsJson) {

        var host =  JSON.parse(messageAsJson.content.content.Host);
        var memory = JSON.parse(messageAsJson.content.content.VirtualMemory);
        var cpuUsage = JSON.parse(messageAsJson.content.content.CpuUsage);
    
        var nodeInfo = {
            "memory_max_usage" : memory.used,
            'cpu_max_usage' : cpuUsage,
            'up_time' : host.uptime,
            'os' : host.os,
            'info': JSON.stringify(messageAsJson.content.content)
        };

        var nodeId = messageAsJson.clientInfo.nodeId;

        var socketEmitContent = {
            'memory_usage' : memory.used,
            'cpu_usage' : cpuUsage,
            'up_time' : host.uptime,
            'os' : host.os
        };
        if(!(nodeId in nodeInfoStorage))
            nodeInfoStorage[nodeId] = nodeInfo;
        else {
            nodeInfoStorage[nodeId].memory_max_usage = nodeInfoStorage[nodeId].memory_max_usage < nodeInfo.memory_max_usage ? nodeInfo.memory_max_usage : nodeInfoStorage[nodeId].memory_max_usage;
            nodeInfoStorage[nodeId].cpu_max_usage = nodeInfoStorage[nodeId].cpu_max_usage < nodeInfo.cpu_max_usage ? nodeInfo.cpu_max_usage : nodeInfoStorage[nodeId].cpu_max_usage;
        }

        var channelName = getChannelName(OS_MONITOR_CHANNEL, messageAsJson.clientInfo.clientId, nodeId);

        socketIO.emit(channelName, socketEmitContent);        
    }

    function _logMessage(messageAsJson) {

        var log = {
            "application_id" : messageAsJson.clientInfo.appId,
            "app" : messageAsJson.content.content.app,
            "log_type": messageAsJson.content.content.type,
            "message": messageAsJson.content.content.msg,
            "class" : messageAsJson.content.content.class,
            "date" : messageAsJson.content.content.date,
            "time": messageAsJson.content.content.time.replace(",", ".")
        };

        var applicationLog = require('../../models/ApplicationLog');
                
        var socketEmitContent = {
            'log_type' : log.log_type,
            'class' : log.class,
            'app' : log.app
        };

        var channelName = getChannelName(LOGS_MONITOR_CHANNEL, messageAsJson.clientInfo.clientId, log.application_id);
        
        if(!(channelName in emitSocket))
            emitSocket[channelName] = [];

        emitSocket[channelName].push(socketEmitContent);
    }

    function _connectNode(messageAsJson) {

        var monitoredNode = {
            "clientId":  messageAsJson.clientInfo.clientId,
            "clientSecret" : messageAsJson.clientInfo.clientSecret,
            "appId" : messageAsJson.clientInfo.appId,
            "nodeId" : messageAsJson.clientInfo.nodeId,
            "socketId": messageAsJson.socketId
        };
        
        var MonitoredNode = require('../../models/MonitoredNode');

        MonitoredNode.insert(monitoredNode, (error, results, fields) => {
            if (error) throw error;
        })        
    }

    function _disconnectNode(messageAsJson) {
        var monitoredNode = {
            "socketId": messageAsJson.socketId
        };
        
        var MonitoredNode = require('../../models/MonitoredNode');

        MonitoredNode.remove(monitoredNode, (error, results, fields) => {
            if (error) throw error;
        })        
    }

    
    return {        
        'log-message' : function(msg) {
            try{
                var messageAsJson = JSON.parse(msg.content.toString());
            

                var messageType = messageAsJson.content.TYPE;

                switch(messageType) {
                    case 'OS_MONITOR': 
                        _OSMonitor(messageAsJson);
                        break;

                    case 'LOG_MSG':
                        _logMessage(messageAsJson);
                        break;

                    case 'MULESOFT_APPS_DOMAINS':
                        console.log("HERE");
                        break;

                    case 'DISCONNECT_NODE':
                        _disconnectNode(messageAsJson);
                        break;
                    case 'REGISTER_NODE':
                        _connectNode(messageAsJson);
                        break;
                }
            } catch( e) {
            }
            ch.ack(msg);

        }
    };
}

module.exports = ApplicationMessagesHandler;
