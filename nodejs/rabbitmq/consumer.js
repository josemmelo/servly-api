var amqp = require('amqplib/callback_api');

var RabbitConsumer = function(user, password, host, queuesListeners, socketIO) {
    let connectionString = 'amqp://' + user + ':' + password + '@' + host;

    amqp.connect(connectionString, function(err, conn) {
        if(err){
            console.log("Could not connect to rabbit");
            return;
        }
        conn.createChannel(function(err, ch) {
            let rabbitHandlers = require('./handlers/ApplicationMessagesHandler')(ch, socketIO);

            for (var queueName in queuesListeners){
                if (queuesListeners.hasOwnProperty(queueName)) {
                    var handlerName = queuesListeners[queueName];
                    ch.consume('log-message',  rabbitHandlers[handlerName] );        
                }
            }
        });
    });
}

module.exports = RabbitConsumer;