FROM node:8

WORKDIR /home/src/app

COPY ./package.json ./

RUN npm install

RUN npm install -g nodemon

COPY ./ ./

CMD nodemon index.js