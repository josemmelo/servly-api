var express = require('express');
var router = express.Router();


// define the home page route
router.post('/:id/logs', function (req, res) {
  var log = {
    "application_id" : req.params.id,
    "app" : req.body.app,
    "log_type": req.body.type,
    "message": req.body.msg,
    "class" : req.body.class,
    "date" : req.body.date,
    "time": req.body.time.replace(",", ".")
  };

  var applicationLogFilter = require('../models/ApplicationLogFilters');
  var applicationLog = require('../models/ApplicationLog');

  var getFiltersOptions = {
    "whereClause" :  'application_id = ? and filters like ?',
    "parameters" : [log.application_id, "%" + log.log_type + "%"]
  };

  applicationLogFilter.get(getFiltersOptions, (error, results, fields) => {
    if (error) throw error;
    
    if(results.length == 0) {
      res.send({"msg" : "Log type not to be logged."});
      return;
    }

    applicationLog.insert(log, (err, results, fields) => {
        if (err) throw err;
  
        res.send(results);
    });
  });

});


module.exports = router