var express = require('express');
var router = express.Router();
var ioClient = require('socket.io-client');
var socket = ioClient.connect('http://localhost:3001/monitor');

router.put('/:id', function (req, res) {
    var host =  JSON.parse(req.body.Host);
    var memory = JSON.parse(req.body.VirtualMemory);
    var cpuUsage = JSON.parse(req.body.CpuUsage);

    var nodeInfo = {
        "memory_max_usage" : memory.used,
        'cpu_max_usage' : cpuUsage,
        'up_time' : host.uptime,
        'os' : host.os,
        'info': JSON.stringify(req.body)
    };    

    var Node = require('../models/Node');
    Node.update(req.params.id, nodeInfo, (error, results, fields) => {
        if (error) throw error;

        socket.emit('node_info', nodeInfo);
        res.send(results);
    });
});


module.exports = router