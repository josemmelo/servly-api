var ActiveUser = require('../models/ActiveUser')

module.exports = function(io, app, clients) {
    var mon = io.of('/monitor');

    var activeClients = [];


    io.on('connection', function(socket){
        socket.on('client_register', (msg) => {

            var client = {
                clientId : msg.client_id,
                ipAddress: msg.ip,
                userAgent: msg.user_agent
            };
            
            console.log("CLIENT REGISTER " , client);

            if(activeClients.indexOf(client) === -1)
                ActiveUser.insert(client, (error) => {
                    console.log(error);
                });
                
            client['socketId'] = socket.id;

            activeClients.push(client);
        });
        
        socket.on('disconnect', () => {
            var client = {};
            var clients = [];

            for(var i = 0; i < activeClients.length; i++) {
                if(activeClients[i].socketId == socket.id)
                {
                    ActiveUser.remove(activeClients[i], () => {});   
                }else {
                    clients.push(activeClients[i]);
                }
            }

            activeClients = clients;
        });
    });

    return io;
}
