var mysqlConnection = require('../database/mysql')

var ApplicationLogFilters = {
    get : function(config, callback) {
        var selectFilters = 'SELECT * from application_log_filters';
        console.log(config.parameters);
        if(config.whereClause != null)
            selectFilters += ' where ' + config.whereClause;

        mysqlConnection.conn.query(selectFilters, config.parameters, callback);
    }

}

module.exports = ApplicationLogFilters;