var mysqlConnection = require('../database/mysql')

var ApplicationLog = {
    insert : function(applicationLog, callback) {
        var insertLogQuery = "insert into application_logs (application_id, app, log_type, message, class, date, time)";
        insertLogQuery += " values (?,?,?,?,?,?,?)";
        
        mysqlConnection.conn.query(insertLogQuery, Object.values(applicationLog), callback);

        
        var updateApplicationInformationQuery = "update application_info set total_requests=total_requests+1";
        if(applicationLog.log_type == 'ERROR')
            updateApplicationInformationQuery += ", total_errors=total_errors+1";
        
        updateApplicationInformationQuery += " where application_id = ?";

        mysqlConnection.conn.query(updateApplicationInformationQuery, [applicationLog.application_id], () =>{});        
    },

    bulkInsert : function(applicationLog, callback){
        var sql = "insert into application_logs (application_id, app, log_type, message, class, date, time)";
        sql += "values ?";
        

        conn.query(sql, applicationLog, function(err) {
            if (err) throw err;
            conn.end();
        });
    }
}

module.exports = ApplicationLog;