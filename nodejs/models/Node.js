var mysqlConnection = require('../database/mysql')

var Node = {
    update : function(nodeId, node, callback) {
        var updateQuery = "update nodes set memory_max_usage=?, cpu_max_usage=?, up_time=?, os=?, info=?, updated_at=NOW() where id=?";
        
        var values = Object.values(node);
        values.push(nodeId);
        mysqlConnection.conn.query(updateQuery, values, callback);
    },

    select : function(nodeId, callback) {
        var selectQuery = "select * from nodes where id=?";
        
        var values = [];
        values.push(nodeId);
        mysqlConnection.conn.query(selectQuery, values, callback);
    }
}

module.exports = Node;