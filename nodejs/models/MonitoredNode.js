var mysqlConnection = require('../database/mysql')

var MonitoredNode = {
    insert : function(monitoredNode, callback) {
        var insertMonitoredNode = "replace into monitored_nodes (socketId, client_id, client_secret, node_id, app_id, created_at, updated_at)";
        insertMonitoredNode += " values (?,?,?,?,?,?,?)";
        
        var values =  [monitoredNode.socketId, monitoredNode.clientId, 
            monitoredNode.clientSecret, monitoredNode.nodeId, 
            monitoredNode.appId, new Date(), new Date()];

        try{
            mysqlConnection.conn.query(insertMonitoredNode, values, callback);
        }catch(exception){
            
        }
    },

    remove: function(monitoredNode, callback) {
        var deleteActiveUser = "delete from monitored_nodes where socketId = ?";
        
        var values =  [monitoredNode.socketId];
        try{
            mysqlConnection.conn.query(deleteActiveUser, values, callback);
        }catch(exception){
            
        }
    }
}

module.exports = MonitoredNode;