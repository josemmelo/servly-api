var mysqlConnection = require('../database/mysql')

var ActiveUser = {
    insert : function(activeUser, callback) {

        var userIdEqual = activeUser.clientId == null ? 'is' : '=';

        var alreadyHasActiveUser = "select * from active_users where user_id " + userIdEqual+ " ? and ip_address = ? and user_agent = ?";
        var insertActiveUser = "insert into active_users (user_id, ip_address, user_agent, created_at, updated_at)";
        insertActiveUser += " values (?,?,?,?,?)";
        
        var values =  [activeUser.clientId, activeUser.ipAddress, activeUser.userAgent];
        mysqlConnection.conn.query(alreadyHasActiveUser, values, (error, results, fields) => {
            if (error) throw error;

            if(results.length == 0) {
                values.push(new Date());
                values.push(new Date());
                mysqlConnection.conn.query(insertActiveUser, values, callback);
                return;
            }
        });
    },

    remove: function(activeUser, callback) {
        var deleteActiveUser = "delete from active_users where user_id = ?";
        
        var values =  [activeUser.clientId];

        mysqlConnection.conn.query(deleteActiveUser, values, callback);
    }
}

module.exports = ActiveUser;