var app = require('express')();
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('dotenv').config();

require('./bootstrap/bootstrap')();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var http = require('http').Server(app);
var io = require('socket.io')(http, {transports: ['websocket', 'polling', 'flashsocket']});

var monitorIO = require('./realtime/monitor-nodes')(io, app);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
});

var Queues = {
  'log-message' :  'log-message'    
}

var RabbitConsumer = require('./rabbitmq/consumer');
RabbitConsumer(process.env.RABBIT_USER, process.env.RABBIT_PWD, process.env.RABBIT_HOST,Queues, monitorIO);


http.listen(process.env.LISTENING_PORT | 3000, () => console.log('Example app listening on port ' + process.env.LISTENING_PORT + '!'))