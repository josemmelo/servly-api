<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Servly\Helpers\ApplicationsHelper;
use Servly\Models\Application;
use Tests\TestCase;

class ApplicationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListAllApplications()
    {
        // list all application for user
        $applicationsUser = ApplicationsHelper::index(1, null);
        $this->assertNotCount(0, $applicationsUser);

        // list all application for node
        $applicationsNode = ApplicationsHelper::index(1, 1);
        $this->assertNotCount(0, $applicationsNode);

        // node and user applications not equal. user applications > node applications
        $this->assertNotEquals($applicationsNode, $applicationsUser);
        $this->assertGreaterThan(count($applicationsNode), count($applicationsUser));
    }

    public function testUpdateApplication()
    {
        $request = new Request();

        $request->replace(['name' => 'bar']);

        ApplicationsHelper::update($request, 1);
        $application = Application::find(1);
        $this->assertEquals($application->name, 'bar');
    }

    public function testShowApplication()
    {
        $application = ApplicationsHelper::show(1, 1);

        $this->assertNotNull($application);
    }

    public function testApplicationInformation()
    {
        $application = ApplicationsHelper::information(1);

        $this->assertNotNull($application);
        $this->assertNotNull($application->active_services);
    }
}
