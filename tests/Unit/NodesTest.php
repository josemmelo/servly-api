<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Servly\Helpers\NodesHelper;
use Servly\Models\Node;
use Servly\Models\User;
use Tests\TestCase;

class NodesTest extends TestCase
{
    /**
     * Teste node creation
     *
     * @return void
     */
    public function testCreateNode()
    {
        $userId = 1;
        $user = User::find($userId);

        $this->assertCount(2, Node::where('user_id', $userId)->get());

        $node = [
            'name'             => 'ubuntu node',
            'ip'               => 'localhost',
            'machine_name'     => 'ubuntu',
            'os'               => 'ubuntu',
            'cpu_max_usage'    => 12.09,
            'memory_max_usage' => 1231268,
            'up_time'          => 15324,
        ];

        $node = NodesHelper::store($user, $node);
        $this->assertNotNull($node);
        $this->assertEquals($node->name, 'ubuntu node');
        $this->assertCount(3, Node::where('user_id', $userId)->get());
    }

    public function testNodeShow()
    {
        $userId = 1;
        $user = User::find($userId);
        $nodes = Node::where('user_id', $userId)->get();
        $node = NodesHelper::show($user, $nodes[0]->id);

        $this->assertNotNull($node);
        $this->assertEquals($node, $nodes[0]);
    }

    public function testNodeDelete()
    {
        $userId = 1;
        $user = User::find($userId);
        $nodes = Node::where('user_id', $userId)->get();

        $numberNodesBeforeDelete = count($nodes);
        $nodeId = $nodes[0]->id;

        NodesHelper::delete($user, $nodeId);

        $nodes = Node::where('user_id', $userId)->get();
        $numberNodesAfterDelete = count($nodes);

        $this->assertGreaterThan($numberNodesAfterDelete, $numberNodesBeforeDelete);

        $this->assertNull(Node::find($nodeId));
    }


}
