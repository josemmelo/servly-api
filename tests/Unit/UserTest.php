<?php

namespace Tests\Unit;

use Ramsey\Uuid\Uuid;
use Servly\Helpers\UsersHelper;
use Servly\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public $userEmail = 'jose@mail.com';

    /**
     * Test user creation method.
     */
    public function testUserCreation()
    {
        $userInfo = [
            'name'          => 'Jose',
            'password'      => '123123',
            'email'         => $this->userEmail,
            'client_id'     => Uuid::uuid4(),
            'client_secret' => Uuid::uuid4(),
        ];

        UsersHelper::create($userInfo);

        $user = User::where('email', $userInfo['email'])->first();

        $this->assertNotEquals($user, null);
    }

    public function testUserUpdate()
    {
        $userInfo = [
            'name' => 'Newname'
        ];

        $userUpdated = UsersHelper::update(User::find(1), $userInfo);

        $this->assertNotNull($userUpdated);
        $this->assertEquals($userUpdated, 1);
        $newUser = User::find(1);
        $this->assertEquals($newUser->name, 'Newname');
    }

    public function testUserUpdateWithNonValidInfo() {
        $userInfo = [
            'name' => 'Newname',
            'ok' => "OASJD"
        ];

        $userUpdated = UsersHelper::update(User::find(1), $userInfo);

        $this->assertNotNull($userUpdated);
        $this->assertEquals($userUpdated, 1);
        $newUser = User::find(1);
        $this->assertEquals($newUser->name, 'Newname');
    }

    /**
     * Test user client credentials authorization
     *
     * @return void
     */
    public function testUserClientCredentialsAuthorization()
    {
        $user = User::find(1);

        $user = UsersHelper::authorize($user->client_id, $user->client_secret);

        $this->assertNotEquals($user, null);

        $user = UsersHelper::authorize($user->client_id, "123123");

        $this->assertEquals($user, null);
    }
}
