import pika
import os
import threading
import json
import requests

URL = 'http://localhost:3000/api/'
headers = {'Authorization': 'processor 4F13D333-5759-4D6E-8BD1-BA8318938016', "Content-Type": "application/json"}
        
class ConsumerThread(threading.Thread):

    def muleRunningApps(node, appId, data):
        print(" [x] Received MULESOFT_RUNNING_APPS type from " + appId)
        dataToSend = "{\"information\":" + json.dumps(data) + "}"
        response = requests.put(URL + "applications/" + appId, data=dataToSend, headers=headers)
        print response


    def logMsg(node, appId, data):
        print(" [x] Received LOG MSG type from " + appId)
        dataToSend = json.dumps(data["content"])
        response = requests.post(URL + "applications/" + appId + "/logs", data=dataToSend, headers=headers)
        print response

    def osMonitor(node, appId, data):
        print(" [x] Received OS MONITOR type from node " + node)
        dataToSend = json.dumps(data["content"])    
        response = requests.put(URL + "nodes/" + node, data=dataToSend, headers=headers)
        print response

    options={"MULESOFT_RUNNING_APPS" : muleRunningApps, "LOG_MSG":logMsg, "OS_MONITOR" : osMonitor}

    def __init__(self, host, *args, **kwargs):
        super(ConsumerThread, self).__init__(*args, **kwargs)

        self._host = host

    # Not necessarily a method.
    def callback_func(self, channel, method, properties, body):
        print("{} received '{}'".format(self.name, body))

    def run(self):
        credentials = pika.PlainCredentials("guest", "password")
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=credentials))
        channel = connection.channel()

        channel.queue_declare(queue='log-message', durable=True)
        print(' [*] Waiting for messages. To exit press CTRL+C')

        def callback(ch, method, properties, body):
            try:
                data = json.loads(body)

                app = data["clientInfo"]["appId"]
                node = data["clientInfo"]["nodeId"]

                messageType = data["content"]["TYPE"]

                self.options[messageType](node, app, data["content"])

            except ValueError:
                print 'Decoding JSON has failed'

            ch.basic_ack(delivery_tag = method.delivery_tag)

        channel.basic_qos(prefetch_count=10)
        channel.basic_consume(callback,
                              queue='log-message')

        channel.start_consuming()



if __name__ == "__main__":
    threads = [ConsumerThread("host1")]
    for thread in threads:
        try:
            thread.start()  
        except (KeyboardInterrupt, SystemExit):
            cleanup_stop_thread()
            sys.exit()
